"""Tests for salvus_toolbox."""

import os
from pathlib import Path
from typing import List, Tuple

import h5py
import numpy as np
import pytest
import xarray as xr

import salvus_mesh.simple_mesh as bm
import salvus_toolbox.ses3d as ses3d
import salvus_toolbox.toolbox as st
import salvus_toolbox.toolbox_geotech as st_geo


def xarray_setup_spherical(
    lat: Tuple[float], lon: Tuple[float], rad: Tuple[float]
) -> xr.Dataset:

    lat = np.linspace(lat[0], lat[1], 50)
    lon = np.linspace(lon[0], lon[1], 100)
    rad = np.linspace(rad[0], rad[1])

    vs, vp, rho = np.meshgrid(lat, lon, rad, indexing="ij")
    return xr.Dataset(
        {
            "vs": (["latitude", "longitude", "radius"], vs),
            "vp": (["latitude", "longitude", "radius"], vp),
            "rho": (["latitude", "longitude", "radius"], rho),
        },
        coords={"latitude": lat, "longitude": lon, "radius": rad},
    )


def xarray_setup_cartesian(
    min_x: Tuple[float], dx: Tuple[float], nx: Tuple[int], pars: List[str]
):
    """Generate a simple xarray.Dataset for testing.

    Parameters
    ----------
    min_x : Tuple[float]
        Dim-sized array of minimum coordinate values.
    dx : Tuple[float]
        Dim-sized array of spacings.
    pars : List[str]
        List of parameters to put on mesh.

    """
    dim = len(min_x)
    coord_vals = tuple([np.arange(nx[_i]) * dx[_i] for _i in range(dim)])
    coord_names = tuple([chr(i) for i in range(ord("x"), ord("x") + dim)])

    coords = {n: v for n, v in zip(coord_names, coord_vals)}
    ds = xr.Dataset(coords=coords)

    vals = np.tile(np.arange(1, nx[0] + 1), (*nx[::-2], 1))
    for _i, p in enumerate(pars):
        ds[p] = (coord_names, vals * (_i + 1))

    return ds


@pytest.fixture()
def acoustic_dataset():
    """Get an acoustic xarray."""
    nx = (10, 10)
    mx = (0.0, 0.0)
    dx = (1.0, 1.0)
    return xarray_setup_cartesian(mx, dx, nx, ["VP", "RHO"])


@pytest.fixture()
def elastic_dataset():
    """Get an acoustic xarray."""
    nx = (10, 10)
    mx = (0.0, 0.0)
    dx = (1.0, 1.0)
    return xarray_setup_cartesian(mx, dx, nx, ["VP", "RHO", "VS"])


def test_skeleton_regular_2d(acoustic_dataset):
    """Test 2-D simple mesh generation."""
    ds = acoustic_dataset
    um = st.skeleton_regular_2d(ds, "VP", 1, 1)
    assert um.nelem == 9 ** 2

    um = st.skeleton_regular_2d(ds, "VP", 1, 2)
    assert um.nelem == 18 ** 2

    um = st.skeleton_regular_2d(ds, "VP", 2, 1)
    assert um.nelem == 18 ** 2

    assert np.max(um.points[:, 0] == 1.0)
    assert np.max(um.points[:, 1] == 1.0)

    # Check if last mesh is same as auto mesh.
    um_auto = st.mesh_from_xarray(
        data=ds,
        slowest_velocity="VP",
        maximum_frequency=2,
        elements_per_wavelength=1,
    )

    um.change_tensor_order(4)
    assert (um_auto.points == um.points).all()


def test_abs_boundary_2d(acoustic_dataset):
    """Test extruding for absorbing boundaries."""
    with pytest.raises(ValueError, match="^Attempted to extrude"):
        st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1, (["KORBIA"], 0))

    flagged = -10
    um = st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1, (["x0"], 9))
    assert um.points[:, 0].min() == flagged
    assert um.points[:, 1].min() == 0.0
    assert um.points[:, 0].max() == 9.0
    assert um.points[:, 1].max() == 9.0
    assert (
        um.elemental_fields["absorbing_boundaries"][
            um.get_element_centroid()[:, 0] < 0
        ]
        == 1.0
    ).all()

    um = st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1, (["x1"], 9))
    assert um.points[:, 0].min() == 0.0
    assert um.points[:, 1].min() == 0.0
    assert um.points[:, 0].max() == 19.0
    assert um.points[:, 1].max() == 9.0
    assert (
        um.elemental_fields["absorbing_boundaries"][
            um.get_element_centroid()[:, 0] > 9.0
        ]
        == 1.0
    ).all()

    um = st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1, (["y0"], 9))
    assert um.points[:, 0].min() == 0.0
    assert um.points[:, 1].min() == flagged
    assert um.points[:, 0].max() == 9.0
    assert um.points[:, 1].max() == 9.0
    assert (
        um.elemental_fields["absorbing_boundaries"][
            um.get_element_centroid()[:, 1] < 0
        ]
        == 1.0
    ).all()

    um = st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1, (["y1"], 9))
    assert um.points[:, 0].min() == 0.0
    assert um.points[:, 1].min() == 0.0
    assert um.points[:, 0].max() == 9.0
    assert um.points[:, 1].max() == 19.0
    assert (
        um.elemental_fields["absorbing_boundaries"][
            um.get_element_centroid()[:, 1] > 9.0
        ]
        == 1.0
    ).all()

    # Check if last mesh is same as auto mesh.
    um_auto = st.mesh_from_xarray(
        data=acoustic_dataset,
        slowest_velocity="VP",
        maximum_frequency=1,
        elements_per_wavelength=1,
        absorbing_boundaries=(["y1"], 9),
    )

    um.change_tensor_order(4)
    assert (um_auto.points == um.points).all()


def test_interpolation_regular_2d(acoustic_dataset):
    """Test simple element nodal interpolation."""
    um = st.interpolate_2d(
        acoustic_dataset, st.skeleton_regular_2d(acoustic_dataset, "VP", 2, 1)
    )

    compat_mode = any("_0" in k for k in um.elemental_fields.keys())
    if compat_mode:

        key0, key1 = "VP_0", "RHO_0"
        np.testing.assert_allclose(
            um.elemental_fields[key0],
            np.tile(np.linspace(1, 9.5, 18), (18, 1)).flatten(),
        )

        np.testing.assert_allclose(
            um.elemental_fields[key1],
            np.tile(np.linspace(2, 19, 18), (18, 1)).flatten(),
        )

    else:

        key0, key1 = "VP", "RHO"
        np.testing.assert_allclose(
            um.elemental_fields[key0][:, 0],
            np.tile(np.linspace(1, 9.5, 18), (18, 1)).flatten(),
        )

        np.testing.assert_allclose(
            um.elemental_fields[key1][:, 0],
            np.tile(np.linspace(2, 19, 18), (18, 1)).flatten(),
        )


def test_detect_fluid_acoustic(acoustic_dataset):
    """Test detection of fluid."""
    um = st.detect_fluid(
        st.interpolate_2d(
            acoustic_dataset,
            st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1),
        )
    )
    assert (um.elemental_fields["fluid"] == np.ones(um.nelem)).all()

    um = st.detect_fluid(st.skeleton_regular_2d(acoustic_dataset, "VP", 1, 1))
    assert (um.elemental_fields["fluid"] == np.ones(um.nelem)).all()


def test_detect_fluid_elastic(elastic_dataset):
    """Test detection of fluid."""
    um = st.detect_fluid(
        st.interpolate_2d(
            elastic_dataset,
            st.skeleton_regular_2d(elastic_dataset, "VP", 1, 1),
        )
    )
    assert (um.elemental_fields["fluid"] == np.zeros(um.nelem)).all()


def test_get_shotgather():
    def write_hdf5(field: str, nrec: int, nsamp: int, ncmp: int, dim: int):

        with h5py.File("test_shotgather.h5", mode="w") as fh:

            num = nrec * ncmp * nsamp
            data = np.arange(num).reshape(nrec, ncmp, nsamp)
            coords = np.arange(nrec * dim).reshape(nrec, dim)

            permute = np.random.permutation(nrec)

            data[:, :, :] = data[permute, :, :]
            coords[:, :] = coords[permute, :]

            fh[f"point/{field}"] = data
            fh[f"coordinates_ACOUSTIC_point"] = coords
            fh[f"coordinates_ELASTIC_point"] = coords
            fh["point"].attrs["sampling_rate_in_hz"] = [2]

    # 2D acoustic
    nrec, nts, ncmp, dim = 20, 10, 1, 2
    write_hdf5("phi", nrec, nts, ncmp, dim)
    d, t, e = st.get_shotgather("test_shotgather.h5", field="phi")

    assert t == 0.5
    assert e == [0, (nrec - 1) * dim, t * nts, 0]
    assert (d == np.arange(nrec * nts).reshape(nrec, nts).transpose()).all()

    # 3D acoustic
    nrec, nts, ncmp, dim = 20, 10, 1, 3
    write_hdf5("phi", nrec, nts, ncmp, dim)
    d, t, e = st.get_shotgather("test_shotgather.h5", field="phi")

    assert t == 0.5
    assert e == [0, (nrec - 1) * dim, t * nts, 0]
    assert (d == np.arange(nrec * nts).reshape(nrec, nts).transpose()).all()

    # 2D elastic.
    nrec, nts, ncmp, dim = 20, 10, 2, 2
    write_hdf5("phi", nrec, nts, ncmp, dim)
    d, t, e = st.get_shotgather("test_shotgather.h5", field="phi")

    assert t == 0.5
    assert e == [0, (nrec - 1) * dim, t * nts, 0]
    expected = np.squeeze(
        np.arange(nrec * nts * ncmp)
        .reshape(nrec, ncmp, nts)[:, 0, :]
        .transpose()
    )
    assert (d == expected).all()

    # 2D elastic.
    nrec, nts, ncmp, dim = 20, 10, 2, 2
    write_hdf5("phi", nrec, nts, ncmp, dim)
    d, t, e = st.get_shotgather("test_shotgather.h5", field="phi", cmp=1)

    assert t == 0.5
    assert e == [0, (nrec - 1) * dim, t * nts, 0]
    expected = np.squeeze(
        np.arange(nrec * nts * ncmp)
        .reshape(nrec, ncmp, nts)[:, 1, :]
        .transpose()
    )
    assert (d == expected).all()

    # 3D elastic.
    nrec, nts, ncmp, dim = 20, 10, 3, 2
    write_hdf5("phi", nrec, nts, ncmp, dim)
    d, t, e = st.get_shotgather("test_shotgather.h5", field="phi")

    assert t == 0.5
    assert e == [0, (nrec - 1) * dim, t * nts, 0]
    expected = np.squeeze(
        np.arange(nrec * nts * ncmp)
        .reshape(nrec, ncmp, nts)[:, 0, :]
        .transpose()
    )
    assert (d == expected).all()

    # 3D elastic.
    nrec, nts, ncmp, dim = 20, 10, 3, 2
    write_hdf5("phi", nrec, nts, ncmp, dim)
    d, t, e = st.get_shotgather("test_shotgather.h5", field="phi", cmp=2)

    assert t == 0.5
    assert e == [0, (nrec - 1) * dim, t * nts, 0]
    expected = np.squeeze(
        np.arange(nrec * nts * ncmp)
        .reshape(nrec, ncmp, nts)[:, 2, :]
        .transpose()
    )
    assert (d == expected).all()

    # Bad field
    with pytest.raises(ValueError):
        d, t, e = st.get_shotgather("test_shotgather.h5", field="nobody")

    os.remove("test_shotgather.h5")


def test_mesh_exceptions(acoustic_dataset):

    with pytest.raises(ValueError) as e:

        st.mesh_from_xarray(
            data=acoustic_dataset,
            slowest_velocity="VPP",
            maximum_frequency=2,
            elements_per_wavelength=1,
        )
    assert "Provided parameter" in str(e.value)

    with pytest.raises(ValueError) as e:

        ds = acoustic_dataset.copy()
        ds.coords["p"] = np.ones(10)
        st.mesh_from_xarray(
            data=ds,
            slowest_velocity="VP",
            maximum_frequency=2,
            elements_per_wavelength=1,
        )
    assert "Invalid coordinates" in str(e.value)

    with pytest.raises(NotImplementedError) as e:

        ds = acoustic_dataset.copy()
        ds.coords["z"] = np.ones(10)
        st.mesh_from_xarray(
            data=ds,
            slowest_velocity="VP",
            maximum_frequency=2,
            elements_per_wavelength=1,
        )
    assert "Auto 3-D" in str(e.value)


def test_visualize_wavefield():

    file = Path(__file__).parent / "data" / "output.h5"
    t, d = st.visualize_wavefield_2d(file, "phi")

    with h5py.File(file, "r") as fh:
        expected = np.squeeze(fh["/volume/phi"][:, :, 0, :25]).reshape(
            -1, 2500
        )

    assert (d == expected).all()
    assert (
        str(type(t)) == "<class 'matplotlib.tri.triangulation.Triangulation'>"
    )

    with pytest.raises(ValueError) as e:
        t, d = st.visualize_wavefield_2d(file, "phi_tt")
    assert "Request to plot" in str(e.value)

    with pytest.raises(ValueError) as e:
        t, d = st.visualize_wavefield_2d(file, "karan")
    assert "Unknown field" in str(e.value)

    with pytest.raises(ValueError) as e:
        t, d = st.visualize_wavefield_2d(file, "displacement")
    assert "Request to plot a" in str(e.value)


def test_xarray_spherical(acoustic_dataset):

    ds = xarray_setup_spherical((-22.5, 22.5), (-11.25, 11.25), (4000, 6371))
    mesh = bm.SphericalChunkBuiltIn3D(
        model="prem_iso_no_crust",
        period=100.0,
        elements_per_wavelength=1.25,
        latitude_center=0.0,
        latitude_extent=22.0,
        longitude_center=0.0,
        longitude_extent=10.75,
        minimum_radius_in_km=4500.0,
    )

    mesh = st.interpolate_spherical(ds, mesh.create_mesh(), mode="overwrite")

    nodes = mesh.points
    lat, lon, rad = np.squeeze(np.hsplit(st.cart2sph(nodes), 3))
    np.testing.assert_allclose(
        rad[mesh.connectivity], mesh.elemental_fields["RHO"] * 1000
    )
    np.testing.assert_allclose(
        (90 - np.rad2deg(lat))[mesh.connectivity],
        mesh.elemental_fields["VS"],
        atol=1e-10,
    )
    np.testing.assert_allclose(
        np.rad2deg(lon)[mesh.connectivity],
        mesh.elemental_fields["VP"],
        atol=2e-1,
        rtol=1,
    )

    # Test strange interpolating type.
    with pytest.raises(ValueError) as e:
        st.interpolate_spherical(ds, mesh, mode="xx")
    assert "Unknown interpolation mode" in str(e.value)

    with pytest.raises(ValueError) as e:
        st.mesh_from_xarray_spherical(
            data=acoustic_dataset,
            background_model="",
            minimum_period=100,
            elements_per_wavelength=1,
        )
    assert "Invalid coordinates" in str(e.value)

    ds = xarray_setup_spherical((-22.5, 0), (-11.25, 11.25), (4000, 6300))
    with pytest.raises(ValueError) as e:
        st.mesh_from_xarray_spherical(
            data=ds,
            background_model="prem_iso_no_crust",
            minimum_period=100.0,
            elements_per_wavelength=1.25,
            absorbing_boundaries=0,
        )
    assert "Model is not centered" in str(e.value)


def test_interpolate_spherical_masking():

    # Test masking by layer.
    ds = xarray_setup_spherical((-22.5, 22.5), (-11.25, 11.25), (4000, 6371))
    mesh = bm.SphericalChunkBuiltIn3D(
        model="prem_iso_no_crust",
        period=100.0,
        elements_per_wavelength=1.25,
        latitude_center=0.0,
        latitude_extent=22.0,
        longitude_center=0.0,
        longitude_extent=10.75,
        minimum_radius_in_km=4500.0,
    ).create_mesh()

    nodes = mesh.points
    mesh.element_nodal_fields["RHO"].fill(0)
    mesh = st.interpolate_spherical(ds, mesh, mode="overwrite", layers=-1)
    lat, lon, rad = np.squeeze(np.hsplit(st.cart2sph(nodes), 3))
    nrad = rad[mesh.connectivity]
    lmask = np.logical_and(mesh.elemental_fields["layer"] == 9, True)
    np.testing.assert_allclose(
        nrad[lmask], mesh.elemental_fields["RHO"][lmask] * 1000
    )
    np.testing.assert_allclose(
        0.0, mesh.elemental_fields["RHO"][np.invert(lmask)]
    )


def test_interpolate_spherical_tapering():

    # Test tapering.
    ds = xarray_setup_spherical((-22.5, 22.5), (-11.25, 11.25), (4000, 6371))
    mesh = bm.SphericalChunkBuiltIn3D(
        model="prem_iso_no_crust",
        period=100.0,
        elements_per_wavelength=1.25,
        latitude_center=0.0,
        latitude_extent=22.5,
        longitude_center=0.0,
        longitude_extent=11.25,
        minimum_radius_in_km=4500.0,
    ).create_mesh()

    mesh.element_nodal_fields["RHO"].fill(0)
    ds = st.gen_taper_spherical(ds, 5, 5)
    mesh = st.interpolate_spherical(ds, mesh, mode="overwrite", layers=-1)

    nodes = mesh.points
    lat, lon, rad = np.squeeze(np.hsplit(st.cart2sph(nodes), 3))
    lmask = np.logical_and(mesh.elemental_fields["layer"] == 9, True)

    np.testing.assert_allclose(
        np.max(mesh.elemental_fields["RHO"][lmask]), 6371.0
    )
    np.testing.assert_allclose(
        np.min(mesh.elemental_fields["RHO"][lmask]), 0.0
    )


def test_get_interpolating_splines():

    lx = [
        np.linspace(0.0, 1.0, 10),
        np.linspace(0.0, 1.0, 3),
        np.linspace(0.0, 1.0, 5),
    ]
    ly = [
        np.linspace(10.0, 20.0, 10),
        np.linspace(9.0, 8.0, 3),
        np.linspace(7.0, 1.0, 5),
    ]

    spl = st.get_interpolating_splines(lx, ly, "linear")
    for (t, p) in spl:
        assert t._kind == "linear"
        assert p._kind == "linear"

    spl = st.get_interpolating_splines(
        lx, ly, ["cubic", "linear", "quadratic"]
    )
    for (t, p), (tv, pv) in zip(
        spl, (("spline", "linear"), ("linear", "spline"))
    ):
        assert t._kind == tv
        assert p._kind == pv


def test_generate_mesh_from_splines():

    lx = [
        np.array([0.0, 0.2, 1.0, 2.0, 3.0, 4.0, 4.8, 5.0]) * 1000,
        np.array([0.0, 0.2, 1.0, 2.0, 3.0, 4.0, 4.8, 5.0]) * 1000,
        np.array([0.0, 0.2, 1.0, 2.0, 3.0, 4.0, 4.8, 5.0]) * 1000,
        np.array([0.0, 1.5, 3.5, 5.0]) * 1000,
        np.array([0.0, 2.5, 5.0]) * 1000,
    ]

    ly = [
        np.array([2.0, 2.0, 1.9, 1.7, 2.0, 2.1, 2.0, 2.0]) * 1000,
        np.array([1.6, 1.6, 1.5, 1.4, 1.3, 1.4, 1.5, 1.5]) * 1000,
        np.array([0.5, 0.5, 0.7, 0.6, 1.1, 0.9, 1.2, 1.2]) * 1000,
        np.array([0.2, 0.2, 0.4, 0.4]) * 1000,
        np.array([0.0, 0.0, 0.0]) * 1000,
    ]

    vel = [1000, 2000, 3000, 4000]
    splines = st.get_interpolating_splines(lx, ly, "linear")
    mesh, bnd = st.generate_mesh_from_splines_2d(
        x_min=0,
        x_max=5000,
        splines=splines,
        maximum_frequency=40,
        slowest_velocities=vel,
    )

    assert len(mesh) == 4
    assert bnd == 0.0
    mesh = np.sum(mesh)
    assert mesh.nelem == 20130
    for _i, n in zip(range(4), (9369, 5702, 3477, 1582)):
        assert len(np.where(mesh.elemental_fields["region"] == _i)[0]) == n

    mesh, bnd = st.generate_mesh_from_splines_2d(
        x_min=0,
        x_max=5000,
        splines=splines,
        maximum_frequency=40,
        slowest_velocities=vel,
        use_refinements=False,
    )

    assert len(mesh) == 4
    assert bnd == 0.0
    mesh = np.sum(mesh)
    assert mesh.nelem == 29400
    for _i, n in zip(range(4), (12900, 9900, 4800, 1800)):
        assert len(np.where(mesh.elemental_fields["region"] == _i)[0]) == n

    mesh, bnd = st.generate_mesh_from_splines_2d(
        x_min=0,
        absorbing_boundaries=(["x0", "x1", "y0"], 10),
        x_max=5000,
        splines=splines,
        maximum_frequency=40,
        slowest_velocities=vel,
        use_refinements=False,
    )

    assert len(mesh) == 4
    np.testing.assert_allclose(bnd, 166.66666666666669)
    mesh = np.sum(mesh)
    assert mesh.nelem == 34776
    for _i, n in zip(range(4), (13846, 10626, 5152, 5152)):
        assert len(np.where(mesh.elemental_fields["region"] == _i)[0]) == n

    lx = [
        np.array([0.0, 0.2, 1.0, 2.0, 3.0, 4.0, 4.8, 5.0]) * 1000,
        np.array([0.0, 0.2, 1.0, 2.0, 3.0, 4.0, 4.8, 5.0]) * 1000,
        np.array([0.0, 0.2, 1.0, 2.0, 3.0, 4.0, 4.8, 5.0]) * 1000,
        np.array([0.0, 1.5, 3.5, 5.0]) * 1000,
        np.array([0.0, 2.5, 5.0]) * 1000,
    ]

    ly = [
        np.array([2.0, 2.0, 1.9, 1.7, 2.0, 2.1, 2.0, 2.0]) * 1000,
        np.array([1.6, 1.6, 1.5, 1.4, 1.3, 1.4, 1.5, 1.5]) * 1000,
        np.array([0.5, 0.5, 0.7, 1.5, 1.1, 0.9, 1.2, 1.2]) * 1000,
        np.array([0.2, 0.2, 0.4, 0.4]) * 1000,
        np.array([0.0, 0.0, 0.0]) * 1000,
    ]

    splines = st.get_interpolating_splines(lx, ly, "linear")

    with pytest.raises(ValueError) as e:

        st.generate_mesh_from_splines_2d(
            x_min=0,
            absorbing_boundaries=(["x0", "x1", "y0"], 10),
            x_max=5000,
            splines=splines,
            maximum_frequency=40,
            slowest_velocities=vel,
            use_refinements=False,
        )
    assert "Crossing layers, or layers" in str(e.value)

    # Wrong order.
    ly[1], ly[2] = ly[2], ly[1]
    splines = st.get_interpolating_splines(lx, ly, "linear")

    with pytest.raises(ValueError) as e:
        st.generate_mesh_from_splines_2d(
            x_min=0,
            absorbing_boundaries=(["x0", "x1", "y0"], 10),
            x_max=5000,
            splines=splines,
            maximum_frequency=40,
            slowest_velocities=vel,
            use_refinements=False,
        )
    assert "Interpolating functions are not" in str(e.value)


def test_buildings_2d():

    n_stories = 50
    wall_width = 1.0
    story_height = 3.0
    ceiling_height = 0.3
    building_width = 20.0
    basement_depth = 20.0
    basement_width = 50.0

    f_max = 500.0
    vs_min = 1500.0
    nelem_per_wavelength = 2.0

    mesh, bnd = st_geo.get_simple_building(
        f_max=f_max,
        vs_min=vs_min,
        n_stories=n_stories,
        wall_width=wall_width,
        story_height=story_height,
        basement_width=basement_width,
        basement_depth=basement_depth,
        building_width=building_width,
        ceiling_height=ceiling_height,
        nelem_per_wavelength=nelem_per_wavelength,
    )

    assert mesh.nelem == 2318
    np.testing.assert_allclose(15.0, bnd)
    np.testing.assert_allclose(np.max(mesh.points[:, 0]), 40.0)
    np.testing.assert_allclose(np.max(mesh.points[:, 1]), 150.0)
    np.testing.assert_allclose(np.min(mesh.points[:, 0]), -40.0)
    np.testing.assert_allclose(np.min(mesh.points[:, 1]), -35.0)


def test_read_ses3d():
    model = ses3d.Ses3dModel(
        Path(__file__).parent / "data" / "ses3d", parameters=["vsv", "taper"]
    )

    data = model.data
    np.testing.assert_allclose(
        data["vsv"].data, (np.arange(1, 9) * 1000).reshape((2, 2, 2))
    )
    np.testing.assert_allclose(
        data["taper"].data, np.array([0, 1, 0, 1, 0, 1, 0, 1]).reshape(2, 2, 2)
    )


def test_spherical_taper():

    ds = xarray_setup_spherical((-22.5, 22.5), (-11.25, 11.25), (4000, 6371))
    t = st.gen_taper_spherical(ds, 5, 5)
    _, c = np.unique(t["taper"].data, return_counts=True)

    assert c[0] == 14800
    assert c[-1] == 106400

    assert t["taper"].data.min() == 0.0
    assert t["taper"].data.max() == 1.0

    ds = ds.drop("radius")
    with pytest.raises(ValueError) as e:
        st.gen_taper_spherical(ds, 5, 5)
    assert "An xarray dataset parameterized in" in str(e.value)


def test_geocentric_lat():

    np.testing.assert_allclose(
        st.geographic_to_geocentric_latitude(80), 79.93397880996568
    )
    np.testing.assert_allclose(st.geographic_to_geocentric_latitude(0), 0.0)
    np.testing.assert_allclose(st.geographic_to_geocentric_latitude(90), 90)
    np.testing.assert_allclose(
        st.geographic_to_geocentric_latitude(-10), -9.93439421027913
    )
