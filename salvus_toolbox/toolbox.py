"""
A collection of tools to assist with model and mesh building.
"""

from collections import defaultdict
from pathlib import Path
from typing import List, Optional, Tuple, Union

import h5py
import matplotlib.tri as tri
import numpy as np
import obspy
import xarray as xr
from scipy.interpolate import (
    RectBivariateSpline,
    RectSphereBivariateSpline,
    interp1d,
)

from salvus_mesh import structured_grid_2D, unstructured_mesh
from salvus_mesh.simple_mesh import SphericalChunkBuiltIn3D


def get_shotgather(
    filename: Union[str, Path], field: str, cmp: int = 0, axis: int = 0
) -> np.ndarray:
    """
    Get a shotgather from a Salvus HDF5 receiver file.

    Populates a 2-D numpy array with one component from a Salvus HDF5
    receiver file.

    Args:
        filename: The path to Salvus' HDF5 receiver file.
        field: The name of the field to read (i.e. "displacement", "phi").
        cmp: Index of the component to plot.
        axis: Sort the traces by coordinate value along this axis
            `[0: x, 1: y, 2: z]`.

    Returns:
        Shotgather as a 2-D numpy array.
    """
    acoustic_fields = set(("phi", "phi_t", "phi_tt", "gradient-of-phi"))
    elastic_fields = set(
        (
            "displacement",
            "velocity",
            "acceleration",
            "gradient-of-displacement",
            "strain",
        )
    )

    if field not in acoustic_fields and field not in elastic_fields:
        raise ValueError(f"Unknown field {field}.")
    c_field = "ACOUSTIC" if field in acoustic_fields else "ELASTIC"

    with h5py.File(str(filename), mode="r") as fh:

        gather = fh[f"point/{field}"][:, cmp, :]
        dt = 1 / fh["point"].attrs["sampling_rate_in_hz"][0]
        x = fh[f"coordinates_{c_field}_point"][:, :]

    sorted_gather = gather[x[:, axis].argsort(), :]
    extent = [
        x[:, axis].min(),
        x[:, axis].max(),
        dt * sorted_gather.shape[1],
        0,
    ]
    return sorted_gather.transpose(), dt, extent


def geographic_to_geocentric_latitude(
    lat: float, flattening: float = 1.0 / 298.257223563
) -> float:
    """
    Return a geocentric latitude when given geographic latitude.

    Args:
        lat: Latitude in degrees.
        flattening: Flattening parameters.

    Returns:
        Geocentric latitude in degrees.
    """
    e2 = 2 * flattening - flattening ** 2
    theta = np.deg2rad(90 - lat)
    theta = np.pi / 2.0 - np.arctan((1 - e2) * np.tan(np.pi / 2.0 - theta))
    return np.rad2deg(np.pi / 2 - theta)


def skeleton_regular_2d(
    data: xr.Dataset,
    slowest_velocity: str,
    maximum_frequency: float,
    elements_per_wavelength: float,
    absorbing_boundaries: Optional[Tuple[List[str], float]] = None,
) -> unstructured_mesh.UnstructuredMesh:
    """
    Return a simple rectilinear skeleton mesh from Salvus' mesher.

    Given a string specifying the parameter with the slowest velocity, this
    function will return a mesh which ensures that at least the specified
    number of elements per wavelength are specified given the specified
    maximum frequency. This returns the simplest mesh possible -- no local
    optimizations are done here to improve solver efficiency.

    Args:
        data: xarray dataset describing the rectilinear model to be meshed.
        slowest_velocity: String specifying the parameter in the xarray
            dataset with the slowest velocity.
        maximum_frequency: Maximum frequency for which the
            `elements_per_wavelength` parameter should be respected.
        elements_per_wavelength: Number of elements per maximum minimum
            wavelength (computed globally from `slowest_velocity` and
            `maximum_frequency`).
        absorbing_boundaries: Extend the domain from side sets x by
            approximately y elements, given a tuple of the form (x, y), where
            x is a list of side sets to extrude.

    Returns:
        An unstructured mesh object with no attached parameters.
    """
    # Get the meshing criterion.
    v_critical = data[slowest_velocity].values.min()
    lambda_min = v_critical / maximum_frequency
    min_edge_length = lambda_min / elements_per_wavelength

    # Get meshing parameters.
    min_x, max_x = data["x"].values.min(), data["x"].values.max()
    min_y, max_y = data["y"].values.min(), data["y"].values.max()

    # Ensure that side sets passed make sense.
    if absorbing_boundaries is not None:

        # Sanity check on side sets.
        allowed_ss = {"x0", "x1", "y0", "y1"}
        if not set(absorbing_boundaries[0]).issubset(allowed_ss):
            raise ValueError(
                "Attempted to extrude side sets [ {} ] for absorbing "
                "boundaries. Failed because only the side sets [ {} ] exist "
                "in the mesh.".format(
                    ", ".join(absorbing_boundaries[0]), ", ".join(allowed_ss)
                )
            )

        # Modify boundary sizes.
        bnds, size = absorbing_boundaries[0], absorbing_boundaries[1] + 1
        scaled_size = size * min_edge_length
        if "x0" in bnds:
            min_x -= scaled_size
        if "x1" in bnds:
            max_x += scaled_size
        if "y0" in bnds:
            min_y -= scaled_size
        if "y1" in bnds:
            max_y += scaled_size

    # Compute number of elements in each direction.
    nelem_x = int(np.ceil((max_x - min_x) / min_edge_length))
    nelem_y = int(np.ceil((max_y - min_y) / min_edge_length))

    # Generate mesh.
    um = structured_grid_2D.StructuredGrid2D.rectangle(
        nelem_x=nelem_x,
        nelem_y=nelem_y,
        min_x=min_x,
        max_x=max_x,
        min_y=min_y,
        max_y=max_y,
    ).get_unstructured_mesh()
    um.find_side_sets()

    # Mark absorbing boundaries.
    b_marker = np.zeros(um.nelem)
    xc, yc = np.squeeze(np.hsplit(um.get_element_centroid(), 2))
    b_marker[xc < data["x"].values.min()] = 1
    b_marker[yc < data["y"].values.min()] = 1
    b_marker[xc > data["x"].values.max()] = 1
    b_marker[yc > data["y"].values.max()] = 1
    um.attach_field("absorbing_boundaries", b_marker)

    return um


def interpolate_2d(
    data: xr.Dataset, mesh: unstructured_mesh.UnstructuredMesh
) -> unstructured_mesh.UnstructuredMesh:
    """
    Interpolate a set of 2-D parameters onto a 2-D mesh.

    Interpolate a set of 2-D parameters contained in an xarray dataset onto a
    2-D mesh. Uses spline interpolation courtesy of scipy's
    `RectBivariateSpline` interpolator.

    Args:
        data: 2-D xarray dataset containing the regularly-sampled dataset to
            be interpolated.
        mesh: UnstructuredMesh object on which to interpolate the parameters.

    Returns:
        UnstructuredMesh with parameters interpolated as element-nodal fields.
    """
    # Interpolator requires ascending coordinates.
    dt = data.sortby("y", ascending=True)
    for p, d in dt.items():

        interpolator = RectBivariateSpline(
            dt.coords["x"].data, dt.coords["y"].data, d.T
        )

        mesh_x = mesh.get_element_nodes()[:, :, 0]
        mesh_y = mesh.get_element_nodes()[:, :, 1]
        mesh.attach_field(p, interpolator(mesh_x, mesh_y, grid=False))

    return mesh


def detect_fluid(
    um: unstructured_mesh.UnstructuredMesh
) -> unstructured_mesh.UnstructuredMesh:
    """
    Automatically detect fluid regions.

    Automatically detects where fluid regions are in a coupled mesh, or
    marks the entire mesh as fluid if no elastic-only parameters are found.

    Args:
        um: UnstructuredMesh (with parameters attached) which should be
            inspected for fluid and solid regions.

    Returns:
        UnstructuredMesh object with fluid flag attached.
    """
    compat_mode = any("_0" in k for k in um.elemental_fields.keys())
    if compat_mode:  # pragma: no cover
        vs_str = "VS_0"
    else:
        vs_str = "VS"

    if vs_str not in um.elemental_fields:
        um.attach_field("fluid", np.ones(um.nelem))
        return um

    fluid = np.ones(um.nelem)
    if compat_mode:  # pragma: no cover
        fluid_index = np.where(um.elemental_fields[vs_str] > 0.0)
    else:
        fluid_index = np.where(
            np.any(um.elemental_fields[vs_str] > 0.0, axis=1)
        )
    fluid[fluid_index] = 0
    um.attach_field("fluid", fluid)
    return um


def mesh_from_xarray(
    *,
    data: xr.Dataset,
    slowest_velocity: str,
    maximum_frequency: float,
    elements_per_wavelength: float,
    model_order: int = 4,
    absorbing_boundaries: Optional[Tuple[List[str], float]] = None,
) -> unstructured_mesh.UnstructuredMesh:
    """
    Generate a Salvus mesh from an xarray.Dataset object.

    This function is meant to be used as an entry point and way to
    encapsulate the variety of mesh generation algorithms contained in
    SalvusMesh, and is optimized for regularly-gridded data. The data can be
    either 2- or 3-D, and must be contained within a xarray.Dataset object.
    This object needs to contain coordinate arrays labelled 'x' and 'y', or
    'x', 'y', and 'z' if working in a Cartesian coordinate system. If working
    in a spherical coordinate system as is common in regional to global scale
    seismology, one must provide the parameters 'radius', 'latitude', and
    'longitude'. Material paramters stored as DataArrays within the Dataset
    should be named according to the Salvus parameter naming conventions.

    This function will check the sanity of inputs, and will raise if
    something incorrect is detected. The sophistication and varitey of
    meshing algorithms contained within this function will evolve over time,
    so ensure to check back for updates.

    Args:
        data: xarray Dataset containing the parameters and coordinates of the
            model.
        slowest_velocity: String specifying which parameter within the
            Dataset corresponds to the parameter controlling the slowest
            velocity.
        maximum_frequency: Maximum frequency expected to be present in the
            resultant simulations. Will be used in conjunction with
            `elements_per_wavelength` to determine the critical edge lengths
            of the mesh.
        elements_per_wavelength: Elements per minimum wavelength in the
            model. Will be used in conjunction with `maximum_frequency` to
            determine the critical edge lengths of the mesh. model_order:
            Order of the polynomial interpolation within each element.
        model_order: The polynomial order of the model.
        absorbing_boundaries: Extend the domain from side sets x by
            approximately y elements, given a tuple of the form (x, y), where
            x is a list of side sets to extrude.

    Returns:
        An unstructured mesh object populated with parameters, which is ready
        for use within Salvus.

    Raises:
        ValueError: If the proposed `slowest_velocity` parameter is not one
            of the provided parameters in the xarray Dataset.
        ValueError: If the provided coordinates do not match those specified
            in the docstring.
        NotImplementedError: If a mesh type is selected which is not yet
            implemented within this interface.
    """
    # Check sanity of meshing criterion.
    if slowest_velocity not in data.keys():
        raise ValueError(
            """
        Provided parameter {} for slowest velocity, but this parameter
        is not present in the xarray Dataset.
        Recognized parameters are: [ {} ]""".format(
                slowest_velocity, ", ".join(data.keys())
            )
        )

    # Check sanity of coordinates.
    dim = len(data.coords)
    req_coords = set(("x", "y")) if dim == 2 else set(("x", "y", "z"))
    if not set(data.coords) == req_coords:
        raise ValueError(
            """
        Invalid coordinates present in xarray Dataset. Found {}, but
        require {}""".format(
                set(data.coords), req_coords
            )
        )

    # Generate mesh skeleton.
    if dim == 2:
        um = skeleton_regular_2d(
            data,
            slowest_velocity,
            maximum_frequency,
            elements_per_wavelength,
            absorbing_boundaries,
        )
    else:
        raise NotImplementedError("Auto 3-D meshing coming soon.")

    um.change_tensor_order(model_order)

    # Interpolate parameters.
    um = interpolate_2d(data, um)

    return detect_fluid(um)


def cart2sph(xyz: np.ndarray) -> np.ndarray:
    """
    Transforms cartesian coordinates into spherical coordinates.

    Args:
        xyz: An array of cartesian coordinates, ordered like [pts, dim].

    Returns:
        An array of spherical coordiantes in (colatitude, longitude, radius).
        Degree units are left in radians.
    """
    sph = np.array(np.zeros_like(xyz))
    x = xyz[:, 0] ** 2 + xyz[:, 1] ** 2
    sph[:, 0] = np.arctan2(np.sqrt(x), xyz[:, 2])
    sph[:, 1] = np.arctan2(xyz[:, 1], xyz[:, 0])
    sph[:, 2] = np.sqrt(x + xyz[:, 2] ** 2)
    return sph


def gen_taper_spherical(
    ds: xr.Dataset, deg_lat: float, deg_lon: float
) -> xr.Dataset:
    """
    Generate an approximate lat/lon taper in spherical coordinates.

    Args:
        ds: Template model in spherical coordinates for which to generate the
            taper.
        deg_lat: Size of taper in degrees of latitude.
        deg_lon: float Size of taper in degrees of longitude.

    Returns:
        A new xarray dataset with a new member Dataarray called "taper", which
        can be used to taper the edges of the model.
    """
    if not {"latitude", "longitude", "radius"}.issubset(ds._coord_names):
        raise ValueError(
            "An xarray dataset parameterized in spherical "
            "coordinates is required to use this function."
        )

    mlat, mlon = 1 / deg_lat, 1 / deg_lon
    lat, lon, _ = [
        x.data
        for x in xr.broadcast(ds["latitude"], ds["longitude"], ds["radius"])
    ]

    if deg_lat is not None:
        tlat = np.array(np.ones_like(lat))
        tl0 = np.where(lat - lat.min() < deg_lat)
        tl1 = np.where(lat.max() - lat < deg_lat)
        tlat[tl0] = mlat * (lat[tl0] - lat.min())
        tlat[tl1] = mlat * (lat.max() - lat[tl1])
        tlat = np.clip(tlat, 0, 1)

    if deg_lon is not None:
        tlon = np.array(np.ones_like(lon))
        tl0 = np.where(lon - lon.min() < deg_lon)
        tl1 = np.where(lon.max() - lon < deg_lon)
        tlon[tl0] = mlon * (lon[tl0] - lon.min())
        tlon[tl1] = mlon * (lon.max() - lon[tl1])
        tlon = np.clip(tlon, 0, 1)

    ds["taper"] = (("latitude", "longitude", "radius"), tlat * tlon)
    return ds


def interpolate_spherical(
    data: xr.Dataset,
    mesh: unstructured_mesh.UnstructuredMesh,
    mode: str = "perturbation_to_background",
    spherical_max_radius: float = 6371000.0,
    layers: Optional[List[int]] = None,
) -> unstructured_mesh.UnstructuredMesh:
    """
    Do a model interpolation in spherical coordinates.

    Use scipy.interpolate.RectSphereBivariateSpline to create a spherical
    spline interpolation over the surface of each radial layer. Then use
    linear interpolation between these radial layers to get values at a
    general unstructured depth. Function is vectorized, so it should be
    pretty quick.

    Args:
        data: Xarray dataset with spherical coordinates. Must have latitude,
            longitude, and radius as coordinate arrays. Latitude and longitude
            should be in degrees.
        mesh: Mesh to interpolate onto. Should of course be a spherical chunk
            mesh.
        mode: How to perform the model attachment. Accepts either
            "perurbation_to_background" which adds the provided model to the
            values already present in the mesh, or "overwrite", which replaces
            the values in the mesh with the provided model.
        spherical_max_radius: The spherical radius of the domain, defaults to
            the radius of Earth in m.
        layers: Restrict the interpolation to specific "layers" in the mesh.
            Looks for an elemental_field called "layers", and only interpolates
            onto elements which are part of a particular layer. This field is
            automatically attached by the mesher in Salvus versions greater
            than 0.10.3.

    Returns:
        Mesh with the parameters in the Dataset interpolated onto it.
    """
    # Change coordinates for RectSphereBivariateSpline.
    # u -> colatitude.
    # v -> longitude.
    data_use = data.copy()
    data_use = data_use.assign_coords(
        u=np.deg2rad(90 - data_use.latitude),
        v=np.deg2rad(
            np.remainder(data_use.longitude - data_use.longitude.min(), 360)
        ),
    ).sortby(["radius"], ascending=True)

    # Setup interpolating objects.
    n_rads = len(data_use.coords["radius"])
    r_vals, r_interps = np.empty((n_rads)), defaultdict(list)
    for i in range(n_rads):

        # Get all data at this radius.
        sl = data_use.isel(radius=i)

        # Rescale radius and re-sort colatitude by ascending values.
        r_vals[i] = sl.radius * 1000
        sl = sl.sortby(["u", "v"], ascending=True)

        # Generate interpolation objects.
        for key, val in sl.items():

            # # Perhaps apply taper.
            # if "taper" in sl.keys():
            #     final_data = val.data * sl["taper"].data
            # else:
            final_data = val.data.copy()

            # Construct interpolator.
            r_interps[key.upper()].append(
                RectSphereBivariateSpline(u=sl.u, v=sl.v, r=final_data, s=0)
            )

    # Get nodes and compute radius.
    nodes = mesh.get_element_nodes()
    r_mesh_1d = mesh.element_nodal_fields["z_node_1D"] * spherical_max_radius

    # Compute spherical coordinates.
    x = nodes[:, :, 0] ** 2 + nodes[:, :, 1] ** 2
    r_mesh = np.sqrt(x + nodes[:, :, 2] ** 2)
    u_mesh = np.arctan2(np.sqrt(x), nodes[:, :, 2])
    v_mesh = np.arctan2(nodes[:, :, 1], nodes[:, :, 0])
    mesh.attach_field("radius", r_mesh)

    # Modify v to be 0 -> 360.
    v_mesh -= np.deg2rad(data_use.longitude.data).min()
    v_mesh[v_mesh < 0] += 2 * np.pi

    # Interpolate region by region.
    for _i in range(len(r_vals) - 1):

        # Get region.
        r0, r1 = r_vals[_i], r_vals[_i + 1]
        u0, u1 = data_use.u.data.min(), data_use.u.data.max()
        v0, v1 = data_use.v.data.min(), data_use.v.data.max()
        if _i + 1 == len(r_vals) - 1:
            r1 = np.max(r_mesh_1d)

        # Mask region.
        eps = 1e-6
        mask_u = np.logical_and(u_mesh >= u0, u_mesh <= u1)
        mask_v = np.logical_and(v_mesh >= v0, v_mesh <= v1)
        mask_r = np.logical_and(r_mesh_1d >= r0 - eps, r_mesh_1d <= r1 + eps)
        mask = np.logical_and(np.logical_and(mask_r, mask_u), mask_v)

        # Optionally mask on layer.
        if layers is not None:

            lvar = mesh.elemental_fields["layer"]
            layers = (
                [layers]
                if not isinstance(layers, (list, np.ndarray))
                else layers
            )
            max_layer = np.max(lvar) + 1
            layers = [max_layer + l if l < 0 else l for l in layers]
            mask_l = np.isin(lvar, layers)
            mask = np.logical_and(mask, mask_l[:, np.newaxis])

            # Re-mask on max layer radius.
            if _i + 1 == len(r_vals) - 1:
                max_r_1d_layers = np.max(r_mesh_1d[lvar == max(layers)])
                mask = np.logical_and(mask, r_mesh_1d <= max_r_1d_layers)

        r_chunk, u_chunk, v_chunk = r_mesh_1d[mask], u_mesh[mask], v_mesh[mask]
        r_perc = (r_chunk - r0) / (r1 - r0)

        t_val = 1
        if "TAPER" in r_interps.keys():
            val0t = r_interps["TAPER"][_i + 0](u_chunk, v_chunk, grid=False)
            val1t = r_interps["TAPER"][_i + 1](u_chunk, v_chunk, grid=False)
            t_val = np.clip(r_perc * val1t + (1 - r_perc) * val0t, 0, 1)

        # Interpolate all parameters in region.
        for key, val in r_interps.items():

            if key in ("TAPER", "MASK"):
                continue

            # Interpolate linearly in radius.
            # Interpolate using RectSphereBivariateSpline in t, p.
            val0 = val[_i + 0](u_chunk, v_chunk, grid=False)
            val1 = val[_i + 1](u_chunk, v_chunk, grid=False)
            p_val = r_perc * val1 + (1 - r_perc) * val0

            if mode == "overwrite":
                mesh.element_nodal_fields[key][
                    mask
                ] = p_val * t_val + mesh.element_nodal_fields[key][mask] * (
                    1 - t_val
                )

            elif mode == "perturbation_to_background":
                mesh.element_nodal_fields[key][mask] = (
                    mesh.element_nodal_fields[key][mask] + p_val * t_val
                )

            elif mode == "debug":  # pragma: no cover
                mesh.element_nodal_fields[key][mask] = (
                    np.ones_like(mesh.element_nodal_fields[key][mask])
                ) * r_vals[_i]

            else:
                supported = {
                    "perturbation_to_background",
                    "overwrite",
                    "debug",
                }
                raise ValueError(
                    "Unknown interpolation mode. Supported modes: [ "
                    + ",".join(supported)
                    + " ]."
                )

    return mesh


def mesh_from_xarray_spherical(
    *,
    data: xr.Dataset,
    background_model: str,
    minimum_period: float,
    elements_per_wavelength: float,
    model_order: int = 1,
    absorbing_boundaries: Optional[float] = 0,
) -> unstructured_mesh.UnstructuredMesh:
    """
    Generate a mesh from a regular grid in spherical coordinates.

    Give a model which is regularly sampled in spherical coordiantes, return
    a mesh with those parameters interpolated. The model sould be passed as
    an xarray Dataset with the fast axes being latitude, longitude, and
    radius, respectively (latitude is fastest). The dimensions of the dataset
    should be titled exactly "latitude", "longitude", and "radius". For now
    an approximate interpolation will be done assuming euclidian distance
    between normalized spherical coordinates. This function will also attempt
    to auto-detect and fluid regions present in the model.

    To at least ensure that the interpolation is approximately correct, this
    function requires that the model be centered at a lat/lon of (0, 0).
    Additionally, the chunk should not extend more than about 25 degrees from
    the meridian.

    Args:
        data: Dataset containing parameters to interpolate.
        background_model: Background model from which to get the size function
            for element size.
        minimum_period: Minimum period one wants to simulate in the mesh.
        elements_per_wavelength: Number of elements per wavelength at minimum
            period (1.25+ should be fine).
        model_order: Order of model interpolation -- use a lower order to save
            memory.
        absorbing_boundaries: Add this many extra degrees of lat/lon on each
            edge of the mesh for kosloff boundaries. The default 0 indicates
            Clayton-Enquist conditions.

    Returns:
        A mesh with parameters attached.

    Raises:
        ValueError: If the proper coordinates are not found.
        ValueError: If the model is not centered at (0, 0).
    """
    print(
        "This function has been deprecated and will be removed. "
        "Please use the `SphericalChunkBuiltIn3D` function from the "
        "`simple_mesh` interface instead."
    )

    req_coords = set(("latitude", "longitude", "radius"))
    if not set(data.coords) == req_coords:
        raise ValueError(
            """
        Invalid coordinates present in xarray Dataset. Found {}, but
        require {}""".format(
                set(data.coords), req_coords
            )
        )

    m_rad = float(data["radius"].data.min())
    lat, lon = data["latitude"].data, data["longitude"].data

    c_lat, c_lon = lat.mean(), lon.mean()
    e_lat, e_lon = (lat.max() - lat.min()) / 2, (lon.max() - lon.min()) / 2

    eps = 1e-6
    if abs(c_lat) > eps or abs(c_lon) > eps:
        raise ValueError("Model is not centered at lat/lon (0,0).")

    mesh = SphericalChunkBuiltIn3D(
        model=background_model,
        tensor_order=model_order,
        period=float(minimum_period),
        latitude_center=0.0,
        longitude_center=0.0,
        latitude_extent=float(e_lat) + 2 * absorbing_boundaries,
        longitude_extent=float(e_lon) + 2 * absorbing_boundaries,
        minimum_radius_in_km=m_rad - absorbing_boundaries * 110,
    ).create_mesh()

    lat, lon, rad = np.squeeze(
        np.hsplit(cart2sph(mesh.get_element_centroid()), 3)
    )

    # Visualize (approx) absorbing boundaries.
    bnds = np.zeros(mesh.nelem)
    lat, lon = np.rad2deg(lat), np.rad2deg(lon)
    bnds[lat > (90 + e_lat)] = 1
    bnds[lat < (90 - e_lat)] = 1
    bnds[lon > +e_lon] = 1
    bnds[lon < -e_lon] = 1
    bnds[rad < m_rad * 1000] = 1
    mesh.attach_field("absorbing_boundaries", bnds)

    return mesh


def read_elastic_marmousi(
    directory: Path, parameters: List[str]
) -> xr.Dataset:  # pragma: no cover
    """
    Read the 'AGL Elastic Marmousi model' into an xarray dataset.

    The resulting dataset suitible for use by SalvusMesh. To use another
    SEG-Y file, RSF file, or even your own regularly-gridded format, you need
    to modify the function below to accept your data, while ensuring that the
    structure of the returned xarray.Dataset remains the same.

    Args:
        directory: Path to the 'model' directory of the Elastic Marmousi model.
        parameters: Parameters of the model to read in.

    Returns:
        An xarray Dataset object that can be used to generate a Salvus mesh.
    """
    # Template based on downloaded model folder.
    template = "MODEL_{}_1.25m.segy.tar.gz"
    par_map = {
        "VP": "P-WAVE_VELOCITY",
        "VS": "S-WAVE_VELOCITY",
        "RHO": "DENSITY",
    }

    # Read raw data in using Obspy.
    # We use decimation here to speed up the process.
    raw_data = {}
    decimate = 10
    for par in parameters:

        # Ensure that the file actually exists!
        filename = directory / template.format(par_map[par])
        assert filename.exists(), "Filename {} does not exist!".format(
            filename
        )

        # Read via Obspy into a numpy array.
        stream = obspy.read(str(filename))
        raw_data[par] = np.stack(
            t.data[::decimate] for t in stream.traces[::decimate]
        ).transpose()

    # Initialize the "xarray" dataset which will be used in out
    # simple meshing interface.
    spacing = 1.25 * decimate
    ny, nx = raw_data["VP"].shape

    # Initialize the dataset with some coordiante arrays.
    # Note here we are reversing the y-direction, as we want the origin
    # (0, 0) to be in the bottom left corner of the resulting model.
    data = xr.Dataset(
        coords={
            "x": np.arange(nx) * spacing,
            "y": np.arange(ny - 1, -1, -1) * spacing,
        }
    )

    # Copy the data into xarray's data structure.
    for item, array in raw_data.items():
        data[item] = (("y", "x"), array)

    # Transform density to SI units (kg/m^3).
    data["RHO"] *= 1000
    return data


def visualize_wavefield_2d(
    filename: Union[str, Path], field: str, cmp: int = 0
) -> Tuple[tri.triangulation.Triangulation, np.ndarray]:
    """
    Get an object that can be used to plot simple 2-D wavefields.

    This method works by using the tricontourf method in matplotlib, and so
    comes with some limitations. For instance, if the mesh is not convex, the
    triangulation will fill in any holes and thus lead to unexpected results.
    This function is meant to quickly plot wavefields on simple domains for
    use in jupyter-notebooks and the like -- for more detailed volumetric
    plotting (including in 3-D) please use Paraview.

    Args:
        filename: Name of volumetric output file.
        field: Field to plot.
        cmp: Component of the field to plot.

    Returns:
        A tuple of a triangulation object, followed by the data which can be
        plotted with respect to that object, for instance plt.tricontourf(t,
        d[ts, :]) will plot the output at time-step ts.

    Raises:
        ValueError: If a field is completely unknown in Salvus.
        ValueError: If a field is not present in the file.
        ValueError: If a field is of a different physics than that in the file.
    """
    sf = set(("phi", "phi_t", "phi_tt", "gradient-of-phi"))
    vf = set(
        (
            "displacement",
            "velocity",
            "acceleration",
            "gradient-of-displacement",
            "strain",
        )
    )

    if field not in sf and field not in vf:
        raise ValueError("Unknown field specified.")
    physics = "scalar" if field in sf else "vector"

    crd_base = "/coordinates_{}"
    coord_names = {
        "scalar": crd_base.format("ACOUSTIC"),
        "vector": crd_base.format("ELASTIC"),
    }

    with h5py.File(str(filename), "r") as fh:

        # Read coordinates.
        if coord_names[physics] in fh:
            crds = fh[coord_names[physics]][:]
        else:
            raise ValueError(
                f"Request to plot a {physics} field, but no {physics} fields "
                f"were found in {filename}."
            )

        # Generate triangulation.
        npts, d = np.product(crds.shape[:2]), crds.shape[2]
        triang = tri.Triangulation(
            *np.squeeze(np.hsplit(crds.reshape(npts, d), d))
        )

        v_field = f"/volume/{field}"
        if v_field in fh:
            return (
                triang,
                (np.squeeze(fh[v_field][:, :, cmp, :25])).reshape(-1, npts),
            )
        else:
            raise ValueError(
                f"Request to plot field {field}, but it was not found in "
                f"{filename}."
            )


def get_interpolating_splines(
    layers_x: List[np.ndarray],
    layers_y: List[np.ndarray],
    kind: Union[List[str], str],
) -> List[Tuple[interp1d, interp1d]]:
    """
    Get a list of tuples of interpolating splines from x and y values.

    The list contains pairs of interpolating functions defining the top and
    bottom of a given layer. `layers_x` should be a 1-D numpy array which
    contains the `x` values of spline points, while `layers_y` should be 2-D
    numpy array which contains the `y` values definining each layer. If
    `layers_y` is an array with shape (n, m) then we have n layers which
    are each defined by m tie points.

    For this function, horizons should not cross each other.

    Args:
        layers_x: List of 1-D x values defining the interpolation points.
            Each array in the list defines the x-values for one discontinuity.
        layers_y: List of 1-D y values defining the interpolation points.
            Each array in the list defines the y-values for one discontinuity.
        kind: Interpolation styles to use. If just one value is passed, that
            method will be used for all interpolations. Alternatively, if a
            list of styles is passed in the same order as `layers_x` and
            `layers_y`, then the styles will be individually applied to the
            associated horizon.

    Returns:
        A list of n - 1 tuples defining the distinct regions in the model.
    """
    splines = []
    if type(kind) is str:
        kind = [kind] * len(layers_y)

    for _i in range(len(layers_y) - 1):

        x_top, y_top = layers_x[_i + 0][:], layers_y[_i + 0][:]
        x_bot, y_bot = layers_x[_i + 1][:], layers_y[_i + 1][:]

        fv = "extrapolate"
        interp_top = interp1d(x_top, y_top, kind=kind[_i + 0], fill_value=fv)
        interp_bot = interp1d(x_bot, y_bot, kind=kind[_i + 1], fill_value=fv)
        splines.append((interp_top, interp_bot))

    return splines


def generate_mesh_from_splines_2d(
    *,
    x_min: float,
    x_max: float,
    splines: List[Tuple[interp1d, interp1d]],
    maximum_frequency: float,
    slowest_velocities: List[float],
    model_order: int = 2,
    absorbing_boundaries: Optional[Tuple[List[str], float]] = None,
    elements_per_wavelength: Optional[float] = 1.5,
    use_refinements: bool = True,
    extrude_splines: bool = False,
) -> Tuple[List[unstructured_mesh.UnstructuredMesh], float]:
    """
    Generate a 2-D mesh with interfaces defined by 1-D splines.

    A common use case is to run simulations on a 2-D model with deformed
    internal discontinuities. This function helps to mesh such a model, and
    takes care to add space for absorbing boundaries, and adjust the vertical
    elemenet size within each layer. Given a list of n splines, a mesh with n
    - 1 layers (chunks) will be returned. These chunks can then be summed
    together to create a continuous mesh.

    The splines must be provided in a top down order. That is, the first
    set of points defines the top of the mesh (sideset y1), while the last
    set defines the bottom (sideset y0).


    Args:
        x_min: Minimum x value of the domain (before boundaries).
        x_max: Maximum x value of the domain (before boundaries).
        splines: A list of scipy.interp1d objects which are used to define
            the interfaces in the model. For help generating these splines,
            see the associated "get_interpolating_splines" function.
        maximum_frequency: Maximum frequency which will be simulated in the
            mesh.
        slowest_velocities: A list of the slowest velocities in each layer,
            ordered the same as the interfaces passed to `splines` (i.e. top
            to bottom). These values will be used, along with
            `maximum_frequency` and `elements_per_wavelength` to determine
            the element sizes in each region.
        model_order: The order of the interpolation to use for the parameter
            and shape mapping on each element.
        absorbing_boundaries: Extend the domain from side sets x by
            approximately y elements, given a tuple of the form (x, y), where
            x is a list of side sets to extrude.
        elements_per_wavelength: Elements per wavelength at maximum frequency.
        use_refinements: Create a totally unstrucuted mesh by allowing for
            vertical refinements. Whether or not this will result in a
            performance increase is dependent on the model, we recommend try
            with and without refinements to see what works best for you.
        extrude_splines: Extrapolate the interpolating splines along with the
            absorbing boundaries (if attached).

    Returns:
        A tuple containing of a list of unstructured mesh objects, and
        the boundary size in meters. The unstructured mesh objects can
        be summed together with np.sum() to generate the final mesh, or
        they can be operated on individually.

    Raises:
        ValueError: If layers cross or are deemed too thin.
    """
    # Array to hold structured grids.
    ab = absorbing_boundaries
    regions = []

    if ab is None:
        ab = ("", 0)

    # First, find global slowest velocity. As we are not considering doubling
    # layers, this value will determine the number of elements in the
    # x-direction.
    slowest_global = np.min(slowest_velocities)
    max_edge_x = slowest_global / maximum_frequency / elements_per_wavelength

    # Modify extent for absorbing layers.
    x_min_extrude = x_min - (ab[1] + 1) * max_edge_x * ("x0" in ab[0])
    x_max_extrude = x_max + (ab[1] + 1) * max_edge_x * ("x1" in ab[0])
    nelem_x = int(np.ceil((x_max_extrude - x_min_extrude) / max_edge_x))
    min_bound = ab[1] * max_edge_x

    # Get approximate x locations of element edges.
    x = np.linspace(x_min, x_max, nelem_x + 1)
    x_extrude = np.linspace(x_min_extrude, x_max_extrude, nelem_x * 10)

    # Ensure that splines are ordered correctly.
    t = np.max(splines[0][0](x))
    for (top, bot) in splines:
        t_new = np.max(top(x))
        if t_new > t:
            raise ValueError(
                "Interpolating functions are not properly ordered."
                "They should be ordered from top to bottom."
            )
        t = t_new

    # Now, loop over all layers in the model.
    for _i, (top, bot) in enumerate(splines):

        # Get the velocity in the current layer.
        slowest_local = slowest_velocities[_i]
        max_edge_length_y = (
            slowest_local / maximum_frequency / elements_per_wavelength
        )

        # Do we need to extrude for boundaries?
        abs_top = _i == 0 and "y1" in ab[0]
        abs_bot = _i == len(splines) - 1 and "y0" in ab[0]
        ext = abs_top or abs_bot

        # Get vertical distance at that x location.
        cb, ct = bot(x), top(x)
        distance_y = ct - cb + ext * ab[1] * max_edge_length_y

        # Get number of elements in the y direction for this chunk.
        nelem_y = np.ceil(distance_y / max_edge_length_y).astype(int)

        # Detect if we have crossing of too-thin layers.
        if (nelem_y < 1).any():
            raise ValueError(
                "Crossing layers, or layers which are too thin, were "
                "detected! Please use the appropriate mesh generation wrapper."
            )

        if not use_refinements:
            nelem_y = np.max(nelem_y)

        # Decide how we're going to mesh.
        if not use_refinements:
            chunk_grid = structured_grid_2D.StructuredGrid2D.rectangle
        else:
            chunk_grid = (
                structured_grid_2D.StructuredGrid2D.rectangle_vertical_refine
            )

        # Generate the structured grid for this chunk.
        min_y_chunk = np.max(cb) - abs_bot * ab[1] * max_edge_length_y
        max_y_chunk = np.max(ct) + abs_top * ab[1] * max_edge_length_y
        um = chunk_grid(
            nelem_x=nelem_x,
            nelem_y=nelem_y,
            min_y=min_y_chunk,
            max_y=max_y_chunk,
            min_x=x_min_extrude,
            max_x=x_max_extrude,
        ).get_unstructured_mesh()

        # Tensorize and mark mesh regions.
        um.find_side_sets()
        um.change_tensor_order(model_order)
        um.attach_field("region", np.ones(um.nelem) * _i)

        # Rename internal side sets.
        if _i != 0:
            um.side_sets.update(
                {f"i{len(splines) - 1 - _i}": um.side_sets["y1"]}
            )
            del um.side_sets["y1"]
        if _i != len(splines) - 1:
            del um.side_sets["y0"]

        # Deform top of chunk.
        x_deform = x_extrude if extrude_splines else x
        if np.max(top(x)) != np.min(top(x)):
            dem = top(x_deform) - np.max(top(x_deform))
            um.add_dem_2D(
                x=x_deform,
                dem=dem,
                y0=min_y_chunk,
                yref=np.max(ct),
                y1=np.infty,
            )

        # Deform bottom of chunk.
        if np.max(bot(x)) != np.min(bot(x)):
            dem = bot(x_deform) - np.max(bot(x_deform))
            um.add_dem_2D(
                x=x_deform,
                dem=dem,
                y0=np.min(cb),
                yref=np.max(cb),
                y1=max_y_chunk,
            )

        # See if we need to extend the absorbing boundaries.
        if ext:
            min_bound = min(min_bound, ab[1] * max_edge_length_y)

        um.apply_dem()

        # Mark absorbing boundaries.
        cx, cy = np.squeeze(np.hsplit(um.get_element_centroid(), 2))
        boundaries = np.zeros(um.nelem)
        boundaries[cx < x_min] = 1
        boundaries[cx > x_max] = 1
        boundaries[cy < np.min(bot(x_deform))] = 1
        boundaries[cy > np.max(top(x_deform))] = 1
        um.attach_field("absorbing_boundaries", boundaries)

        um = detect_fluid(um)

        regions.append(um)

    return regions, min_bound
