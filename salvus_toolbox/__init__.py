"""
An open-source package for model handling and various utility functions
around Salvus.

Repository: https://gitlab.com/MondaicSupport/salvus_toolbox/
"""
