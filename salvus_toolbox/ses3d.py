"""
"""
from pathlib import Path
from typing import List, Union

import numpy as np
import pandas as pd
import xarray as xr


def get_par_names(file: str):  # pragma: no cover
    """
    Get the parameter name that a SES3D file is referring to.

    Args:
        file: Filename.

    Returns:
        Parameter name.
    """
    if "rho" in file:
        return "rho"
    elif "vph" in file:
        return "vph"
    elif "vpv" in file:
        return "vpv"
    elif "vsh" in file:
        return "vsh"
    elif "vsv" in file:
        return "vsv"
    elif "taper" in file:
        return "taper"
    raise ValueError(f"Parameter name can not be deduced from {file}.")


class Ses3dModel:
    """
    A class to handle instances of SES3D models.
    """

    def __read_chunk(self, chunk: int, data: np.ndarray):
        index = 0
        for c in range(chunk):
            num_chunk = int(data[index])
            to_return = slice(index + 1, num_chunk + index + 1)
            if c == (chunk - 1):
                return data[to_return]
            index = num_chunk + index + 1  # pragma: no cover

    def __read(self):
        blk_files = [self._directory / f"block_{x}" for x in ["x", "y", "z"]]
        pars = [self._directory / p for p in self._parameters]

        # Get num blocks.
        num_blocks = set()
        for blk in blk_files:
            with open(str(blk), "r") as fh:
                num_blocks.add(fh.readline().strip())
        if len(num_blocks) > 1:  # pragma: no cover
            raise ValueError(
                "Inconsistent number of blocks defined in the "
                "SES3D block files!"
            )
        for b in num_blocks:
            if b != "1":  # pragma: no cover
                raise ValueError(
                    "This function can only handle SES3D models with 1 block."
                )

        self._data = xr.Dataset()

        # Read geometry.
        raw_geom = {}
        for blk in blk_files:
            raw_geom[blk] = np.asarray(pd.read_csv(str(blk), skiprows=0))

        # Read parameters.
        raw_param = {}
        for p in pars:
            raw_param[get_par_names(str(p))] = np.asarray(
                pd.read_csv(str(p), skiprows=0)
            )

        for name, d in raw_geom.items():
            if "block_x" in str(name):
                lat = self.__read_chunk(1, d).flatten()
            elif "block_y" in str(name):
                lon = self.__read_chunk(1, d).flatten()
            elif "block_z" in str(name):
                rad = self.__read_chunk(1, d).flatten()

        for par, d in raw_param.items():
            self._data[par] = (
                ("latitude", "longitude", "radius"),
                self.__read_chunk(1, d).reshape(
                    len(lat) - 1, len(lon) - 1, len(rad) - 1
                ),
            )

        for par, d in raw_param.items():
            if par in ["vsv", "vsh", "vp"]:
                self._data[par] *= 1000

        self._data.coords["latitude"] = (
            "latitude",
            90 - 0.5 * (lat[1:] + lat[:-1]),
        )
        self._data.coords["longitude"] = (
            "longitude",
            0.5 * (lon[1:] + lon[:-1]),
        )
        self._data.coords["radius"] = ("radius", 0.5 * (rad[1:] + rad[:-1]))

    def __init__(self, directory: Union[Path, str], parameters: List[Path]):
        """
        Read and store a SES3D model in spherical coordinates. Models and
        parameters will be automatically detected given a base directory.

        Args:
            directory: Path to model and parameter files.
            parameters: A list of parameters to read.
        """
        directory = Path(directory)
        assert directory.exists(), "Directory {} does not exist!".format(
            directory
        )
        for p in parameters:
            assert (directory / p).exists(), f"File {p} does not exist!"

        files = list(directory.glob("**/*"))
        for blk in ["block_x", "block_y", "block_z"]:
            if not Path(directory) / blk in files:  # pragma: no cover
                raise ValueError(
                    "No SES3D block files found in specified " "directory."
                )

        self._directory = directory
        self._parameters = parameters
        self.__read()

    @property
    def data(self) -> xr.Dataset:
        """
        The model's data.
        """
        return self._data
