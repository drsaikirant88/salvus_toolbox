# SalvusToolbox

An open-source package for model handling and utility functions.

### Documentation

https://mondaic.com/docs/references/python_apis/salvus_toolbox

### Usage

First, install [Salvus](https://www.salvus.io). Then:

```python
conda activate salvus
git clone https://gitlab.com/MondaicSupport/salvus_toolbox.git
cd salvus_toolbox
pip install -v -e .
```
